import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Homescreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';


const AppNavigator = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      header: null
    }
  },
  Home: {
    screen: Homescreen,
    // navigationBarStyle : {navBarHidden: true },
    navigationOptions: {
      header: null
    }
  }
},{
  initialRouteName: 'Login',
});

export default createAppContainer(AppNavigator);