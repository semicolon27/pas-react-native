-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Des 2019 pada 10.21
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mhanan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('a', 'a');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(3) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  `gaji_pokok` int(9) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `kode_jabatan`, `jabatan`, `gaji_pokok`) VALUES
(1, 'KJ-01', 'Direktur', 7000000),
(2, 'KJ-02', 'Manager', 5000000),
(3, 'KJ-03', 'Kabag', 4000000),
(4, 'KJ-04', 'Sekretaris', 3000000),
(5, 'KJ-05', 'Karyawan', 2000000),
(6, 'KJ-06', 'OfficeBoy', 1000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `nip` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `jumlah_anak` int(2) NOT NULL DEFAULT 0,
  `jam_lembur` int(2) NOT NULL DEFAULT 0,
  `tanggal_tambah` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`nip`, `nama`, `kode_jabatan`, `status`, `jumlah_anak`, `jam_lembur`, `tanggal_tambah`) VALUES
('123', 'qeee', 'KJ-04', 1, 2, 2, '2019-12-10 08:30:48'),
('13', 'a', 'KJ-03', 1, 1, 1, '2019-12-10 09:20:09'),
('1970300200102', 'Rizky Aldy Susanto', 'KJ-02', 0, 0, 4, '2019-12-10 07:31:26'),
('1970300200103', 'Ryo Hilmi Ridho', 'KJ-01', 1, 2, 3, '2019-12-10 07:31:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_gaji`
--

CREATE TABLE `log_gaji` (
  `id_log` int(2) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `total_nominal_gaji` int(10) NOT NULL,
  `tanggal_log` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Trigger `log_gaji`
--
DELIMITER $$
CREATE TRIGGER `log_gaji` AFTER INSERT ON `log_gaji` FOR EACH ROW INSERT INTO log_gaji VALUES('', CURRENT_USER, 1000000, CURRENT_TIMESTAMP)
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`),
  ADD UNIQUE KEY `kode_jabatan` (`kode_jabatan`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nip`),
  ADD KEY `kode_jabatan` (`kode_jabatan`);

--
-- Indeks untuk tabel `log_gaji`
--
ALTER TABLE `log_gaji`
  ADD PRIMARY KEY (`id_log`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `log_gaji`
--
ALTER TABLE `log_gaji`
  MODIFY `id_log` int(2) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD CONSTRAINT `karyawan_ibfk_1` FOREIGN KEY (`kode_jabatan`) REFERENCES `jabatan` (`kode_jabatan`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
