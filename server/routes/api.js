const express = require('express');
const router = express.Router();
const karyawan = require("../controllers/karyawanController");

router.post("/login", karyawan.login);

module.exports = router;