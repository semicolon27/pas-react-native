import React, { useState } from 'react'
import { View } from 'react-native'
import { Input, Image, Avatar, Header, Text } from 'react-native-elements'
import Axios from 'axios';
import r from 'rupiah-format'

const HomeScreen = props => {
    const [data, setData] = useState(props.navigation.getParam("data"));
    return (
        <>
         <View style={{height: "20%", flexDirection: "column", paddingTop: 30, backgroundColor: "#4a47eb", padding: 20, justifyContent: "center"}}>
            <Text h4>PT Angin Ribut Sejahtera</Text>
            <Text>Mobile E-Salary</Text>
         </View>
         <View style={{flex: 2, paddingLeft: 20, paddingRight: 20, justifyContent: "center"}}>
             <Text h4>{data.data.nama}</Text>
             <Text h4>{data.data.jabatan.jabatan}</Text>
         </View>
         <View style={{flex: 5, paddingLeft: 20, paddingRight: 20}}>
             <View></View>
             <View style={{flexDirection: "row"}}>
                 <View style={{flex: 1}}>
                     <Text>Gaji Pokok</Text>
                     <Text>Status</Text>
                     <Text>Jumlah Anak</Text>
                     <Text>Jumlah Tunjangan</Text>
                     <Text>Lembur</Text>
                     <Text>Total</Text>
                 </View>
                 <View style={{flex: 1, justifyContent: "flex-start"}}>
                     <Text>{r.convert(data.data.jabatan.gaji_pokok)}</Text>
                     <Text>{data.data.status === 1 ? "Menikah" : "Belum Menikah"}</Text>
                     <Text>{data.data.jumlah_anak}</Text>
                     <Text>{r.convert((parseInt(data.data.jabatan.gaji_pokok,10) * 15/100) * (parseInt(data.data.jumlah_anak,10) < 3 ? parseInt(data.data.jumlah_anak,10) : 2))}</Text>
                     <Text>{r.convert((parseInt(data.data.jabatan.gaji_pokok,10) * 1/100) * 20)}</Text>
                     <Text>{
                         r.convert(((parseInt(data.data.jabatan.gaji_pokok,10) * 15/100) * (parseInt(data.data.jumlah_anak,10) < 3 ? data.data.jumlah_anak : 2))+
                         ((parseInt(data.data.jabatan.gaji_pokok,10) * 1/100) * 20) +
                         parseInt(data.data.jabatan.gaji_pokok,10))
                     }</Text>
                 </View>
             </View>
         </View>
        </>
    )
}

export default HomeScreen;