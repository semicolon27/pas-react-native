'use strict';
module.exports = (sequelize, DataTypes) => {
    const karyawan = sequelize.define('karyawan', {
        nip: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: true,
            type: DataTypes.STRING,
        },
        nama: DataTypes.STRING,
        kode_jabatan: DataTypes.STRING,
        status: DataTypes.INTEGER,
        jumlah_anak: DataTypes.INTEGER,
        jam_lembur: DataTypes.INTEGER,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'karyawan',
    });
    karyawan.associate = function(models) {
        // associations can be defined here
    };
    return karyawan;
};
