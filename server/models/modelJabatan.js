'use strict';
module.exports = (sequelize, DataTypes) => {
    const jabatan = sequelize.define('jabatan', {
        kode_jabatan: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: true,
            type: DataTypes.STRING,
        },
        // kode_jabatan: DataTypes.STRING,
        jabatan: DataTypes.STRING,
        gaji_pokok: DataTypes.INTEGER,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'jabatan',
    });
    jabatan.associate = function(models) {
        // associations can be defined here
    };
    return jabatan;
};
