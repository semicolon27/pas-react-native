const karyawan = require("../models/index").karyawan
const jabatan = require("../models/index").jabatan

karyawan.belongsTo(jabatan, { foreignKey: 'kode_jabatan' });

exports.login = async (req, res) => {
    try{
        const data = await karyawan.findOne({
            include: [jabatan],
            where: {
                nip: req.body.nip,
            }
        });
        if(data !== null){
            res.json({
                data: data,
                message: "Berhasil login"
            }, 201)
        }else{
            res.json({
                message: "NIP tidak ditemukan"
            }, 204)
        }
    }catch(err){
        res.json({
            message: err
        }, 500)
    }
}