import React, { useState } from 'react'
import { View, TouchableHighlight, KeyboardAvoidingView } from 'react-native'
import { Input, Image, Avatar, Text } from 'react-native-elements'
import Axios from 'axios';

export default LoginScreen = props => {
  const [isLoading, setLoading] = useState(false)
  const [err, setErr] = useState(false)
  const [message, setMessage] = useState(null);
  const [nip, setNip] = useState("")
  const handleLogin = async () => {
    setLoading(true)
    let res = await Axios.post("http://172.16.1.74:3000/api/login", {nip: nip});
    setLoading(false)
    if(res.status === 201){
      props.navigation.navigate('Home',{
        data: res.data,
        status: res.status
      })
      setErr(false)
    }else if(res.status === 500){
      setErr(true)
      setMessage("Maaf, server sedang bermasalah")
    }else{
      setErr(true)
    }
  }
  return (
    <>
      <View style={{flex: 2, justifyContent: "center", alignItems: "center"}}>
        <Text h4>
          PT Angin Ribut Sejahtera
        </Text>
        <Text h5>
          Mobile E-Salary
        </Text>
      </View>
      <KeyboardAvoidingView style={{flex: 2, flexDirection: "column" }} behavior='padding' enabled>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Text h5>{err ? "Username atau Password salah" : message !== null ? "Masukan NIP" : message}</Text>
        </View>
        <View style={{padding: 30}}>
          <Input onChangeText={v => setNip(v)} placeholder='NIP' leftIcon={{ name: 'email', color:"#4A47EB", paddingRight: 10 }} />
        </View>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
        <TouchableHighlight style={{height: 20, width: "80%", backgroundColor: "#4A47EB",justifyContent: "center", alignItems: "center", padding: 20, borderRadius: 20}} onPress={handleLogin}>
         <Text style={{fontSize:15, color: 'white'}}>{isLoading ? 'Loading gan...' : 'Login'}</Text>
        </TouchableHighlight>
        </View>
      </KeyboardAvoidingView>
      <View style={{flex: 2,justifyContent: "center", alignItems: "center", padding: 30}}>
        <Text style={{textAlign: "justify"}}>
          Pada aplikasi ini anda dapat melihat gaji anda pada bulan ini.
        </Text>
        <Text>
          Cukup masukan NIP anda pada form diatas.
        </Text>
      </View>
    </>
  )
} 

